# Maratona Tracker

O Maratona Tracker é uma plataforma web que permite ao aluno registrar suas atividades realizadas no contexto de estudos para competições de programação competitiva. Facilita o acompanhamento e análise de seu desempenho, além de permitir gerar relatórios úteis.

### Funcionalidades

- Integração com a API do [Codeforces](https://codeforces.com/), para importar informações dos problemas e contests realizados na plataforma.
- Registro de feedback para contests ou problemas, auxiliando no momento do upsolving.
- Relatórios em markdown contendo histórico de atividades, ideal para disciplina a MAC0214 (Atividade Curricular em Cultura e Extensão) do IME-USP.
- Visualização por meio de gráficos da evolução em diferentes métricas (conteúdos, frequência, rating, entre outras).

### Build e run

Essencialmente, o front-end do projeto pode ser executado através do comando `./gradlew jsRun` (que responderá no endereço `http://localhost:3000`) e o back-end através do comando `./gradlew jvmRun`. Mais informações podem ser encontradas na [Wiki](https://gitlab.uspdigital.usp.br/mac0350-mineiros/maratona-tracker/-/wikis/Build-e-run).

### Tecnologias

- Kotlin 1.9.22
- JVM 17
- Kvision 7.4.1
- Ktor 2.3.8

Para banco de dados:
- Exposed 0.50.1
- H2 2.2.224
- HikariCP 5.1.0

Consulte também nossa [Wiki](https://gitlab.uspdigital.usp.br/mac0350-mineiros/maratona-tracker/-/wikis/home) para mais informações.

### Créditos
Desenvolvido por Marcelo Machado Lage e Otávio de Oliveira Silva na disciplina de Introdução ao Desenvolvimento de Sistemas de Software (MAC0350), ministrada pelo Prof. Dr. Paulo Meirelles em 2024/1.

### Licença
GNU General Public License v3.0 or later
