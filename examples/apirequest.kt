// https://kotlinlang.org/docs/serialization.html
// https://square.github.io/okhttp/#get-a-url

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonElement
import okhttp3.OkHttpClient
import okhttp3.Request

@Serializable
data class Raw(val status: String,
               val result: List<JsonElement>)

@Serializable
data class User(val lastName: String = "",
                val country: String,
                val lastOnlineTimeSeconds: Int,
                val rating: Int,
                val friendOfCount: Int,
                val titlePhoto: String,
                val handle: String,
                val avatar: String,
                val firstName: String = "",
                val contribution: Int,
                val organization: String,
                val rank: String,
                val maxRating: Int,
                val registrationTimeSeconds: Int,
                val maxRank: String)

@Serializable
data class UserCompact(val rating: Int,
                       val handle: String,
                       val rank: String)

private val json = Json { ignoreUnknownKeys = true } // pra ignorar classes que não existem na classe

val client = OkHttpClient()

fun cfApiCall(url: String): String {
    val request = Request.Builder()
        .url(url)
        .build()

    val response = client.newCall(request).execute()

    val raw = json.decodeFromString<Raw>(response.body?.string() ?: """{status:"FAILED",result:[]}""")

    return raw.result.toString().removePrefix("[").removeSuffix("]")
}

fun main() {
    val content = cfApiCall("https://codeforces.com/api/user.info?handles=pastel&checkHistoricHandles=false")

    val obj = json.decodeFromString<User>(content)
    println(obj)

    val objc = json.decodeFromString<UserCompact>(content)
    println(objc)
}