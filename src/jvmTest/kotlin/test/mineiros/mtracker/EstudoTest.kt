import mineiros.mtracker.models.Estudo
import mineiros.mtracker.models.Usuario
import org.junit.Test
import kotlin.test.assertEquals

class EstudoTest {
    val user = Usuario("Paulo", "rmm")
    var meuEstudo = Estudo(user, 60)
    @Test
    fun testGetTempoEstudo() {
        assertEquals(60, meuEstudo.tempo)
    }
}