import kotlinx.coroutines.runBlocking
import mineiros.mtracker.models.Contest
import mineiros.mtracker.models.Problema
import mineiros.mtracker.models.db.*
import mineiros.mtracker.models.db.DatabaseSingleton.createHikariDataSource
import mineiros.mtracker.models.*
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.Assert.assertEquals
import org.junit.Test

class ContestTest {
    val problema1 = Problema(id = "4")
    val problema2 = Problema(id = "5")
    val problemas = listOf(problema1, problema2)
    val contest = Contest(500, problemas)
    @Test
    fun testId() {
        assertEquals(500, contest.id)
    }
    @Test
    fun testProblemas() {
        assertEquals(problemas, contest.problemas)
    }
}

fun initCleanContestMethods() {
    val driverClassName = "org.h2.Driver"
    val jdbcURL = "jdbc:h2:file:./build/db/test"
    val database = Database.connect(createHikariDataSource(jdbcURL, driverClassName))
    transaction(database){
        SchemaUtils.create(ContestsInfo)
        ContestsInfo.deleteAll()
    }
}

class ContestMethodsTest {
    val contest1 = Contest(id = 1951, listOf<Problema>())
    val contest2 = Contest(id = 20000, listOf<Problema>())
    val nome1 = "Codeforces Some Round"
    val nome2 = "A Very Good ICPC Contest From A Very Interesting Region Probably in Europe Having A Unnecessarily Large Name"
    val duracao = 10000

    init {
        initCleanContestMethods()
        runBlocking {
            ContestsInfoService().addContest(contest1.id, nome1, duracao)
            ContestsInfoService().addContest(contest2.id, nome2, 2*duracao)
        }
    }

    @Test
    fun testAllMethods() = runBlocking {
        assertEquals(nome1, contest1.getNome())
        assertEquals(duracao, contest1.getDuracaoS())

        assertEquals(nome2, contest2.getNome())
        assertEquals(2*duracao, contest2.getDuracaoS())
    }
}