package test.mineiros.mtracker.codeforces

import mineiros.mtracker.models.codeforces.*
import org.junit.Test
import org.junit.Assert.*

const val mockRoundJSON =
    "[{ \"id\":255719746, \"contestId\":1951, \"creationTimeSeconds\":1712591959, \"relativeTimeSeconds\":2147483647, \"problem\":{ \"contestId\":1951, \"index\":\"E\", \"name\":\"No Palindromes\", \"type\":\"PROGRAMMING\", \"points\":2250.0, \"tags\":[\"brute force\", \"constructive algorithms\", \"divide and conquer\", \"greedy\", \"hashing\", \"implementation\", \"math\", \"strings\"] }, \"author\":{ \"contestId\":1951, \"members\":[{ \"handle\":\"pastel\" }], \"participantType\":\"PRACTICE\", \"ghost\":false, \"startTimeSeconds\":1712414100 }, \"programmingLanguage\":\"C++17 (GCC 7-32)\", \"verdict\":\"OK\", \"testset\":\"TESTS\", \"passedTestCount\":56, \"timeConsumedMillis\":155, \"memoryConsumedBytes\":30003200 }, { \"id\":255356909, \"contestId\":1951, \"creationTimeSeconds\":1712424444, \"relativeTimeSeconds\":10344, \"problem\":{ \"contestId\":1951, \"index\":\"E\", \"name\":\"No Palindromes\", \"type\":\"PROGRAMMING\", \"points\":2250.0, \"tags\":[\"brute force\", \"constructive algorithms\", \"divide and conquer\", \"greedy\", \"hashing\", \"implementation\", \"math\", \"strings\"] }, \"author\":{ \"contestId\":1951, \"members\":[{ \"handle\":\"pastel\" }], \"participantType\":\"CONTESTANT\", \"ghost\":false, \"room\":500, \"startTimeSeconds\":1712414100 }, \"programmingLanguage\":\"C++17 (GCC 7-32)\", \"verdict\":\"WRONG_ANSWER\", \"testset\":\"PRETESTS\", \"passedTestCount\":1, \"timeConsumedMillis\":46, \"memoryConsumedBytes\":102400 }, { \"id\":255344783, \"contestId\":1951, \"creationTimeSeconds\":1712422541, \"relativeTimeSeconds\":8441, \"problem\":{ \"contestId\":1951, \"index\":\"D\", \"name\":\"Buying Jewels\", \"type\":\"PROGRAMMING\", \"points\":1750.0, \"tags\":[\"constructive algorithms\", \"greedy\", \"math\"] }, \"author\":{ \"contestId\":1951, \"members\":[{ \"handle\":\"pastel\" }], \"participantType\":\"CONTESTANT\", \"ghost\":false, \"room\":500, \"startTimeSeconds\":1712414100 }, \"programmingLanguage\":\"C++17 (GCC 7-32)\", \"verdict\":\"OK\", \"testset\":\"TESTS\", \"passedTestCount\":6, \"timeConsumedMillis\":77, \"memoryConsumedBytes\":0 }]"

val mockRoundSubmissions : List<CFSubmissionCompact> = listOf(
    CFSubmissionCompact(contestId=1951, creationTimeSeconds = 1712591959, problem= CFProblemCompact(index="E", name="No Palindromes", tags= listOf("brute force", "constructive algorithms", "divide and conquer", "greedy", "hashing", "implementation", "math", "strings")), verdict="OK"),
    CFSubmissionCompact(contestId=1951, creationTimeSeconds = 1712424444, problem= CFProblemCompact(index="E", name="No Palindromes", tags= listOf("brute force", "constructive algorithms", "divide and conquer", "greedy", "hashing", "implementation", "math", "strings")), verdict="WRONG_ANSWER"),
    CFSubmissionCompact(contestId=1951, creationTimeSeconds = 1712422541, problem= CFProblemCompact(index="D", name="Buying Jewels", tags= listOf("constructive algorithms", "greedy", "math")), verdict="OK")
)

const val mockGymJSON =
    "[{\"id\":256352447,\"contestId\":105020,\"creationTimeSeconds\":1712939730,\"relativeTimeSeconds\":17490,\"problem\":{\"contestId\":105020,\"index\":\"L\",\"name\":\"Black and White Tree\",\"type\":\"PROGRAMMING\",\"tags\":[]},\"author\":{\"contestId\":105020,\"members\":[{\"handle\":\"pastel\"},{\"handle\":\"eu_caue\"},{\"handle\":\"PMiguelez\"}],\"participantType\":\"VIRTUAL\",\"teamId\":156328,\"teamName\":\"a gente tem muito tempo\",\"ghost\":false,\"startTimeSeconds\":1712922240},\"programmingLanguage\":\"C++17 (GCC 7-32)\",\"verdict\":\"OK\",\"testset\":\"TESTS\",\"passedTestCount\":134,\"timeConsumedMillis\":436,\"memoryConsumedBytes\":54784000},{\"id\":256343662,\"contestId\":105020,\"creationTimeSeconds\":1712938759,\"relativeTimeSeconds\":16519,\"problem\":{\"contestId\":105020,\"index\":\"D\",\"name\":\"Beautiful decrease\",\"type\":\"PROGRAMMING\",\"tags\":[]},\"author\":{\"contestId\":105020,\"members\":[{\"handle\":\"pastel\"},{\"handle\":\"eu_caue\"},{\"handle\":\"PMiguelez\"}],\"participantType\":\"VIRTUAL\",\"teamId\":156328,\"teamName\":\"a gente tem muito tempo\",\"ghost\":false,\"startTimeSeconds\":1712922240},\"programmingLanguage\":\"C++17 (GCC 7-32)\",\"verdict\":\"WRONG_ANSWER\",\"testset\":\"TESTS\",\"passedTestCount\":10,\"timeConsumedMillis\":61,\"memoryConsumedBytes\":4096000},{\"id\":256342259,\"contestId\":105020,\"creationTimeSeconds\":1712938600,\"relativeTimeSeconds\":16360,\"problem\":{\"contestId\":105020,\"index\":\"D\",\"name\":\"Beautiful decrease\",\"type\":\"PROGRAMMING\",\"tags\":[]},\"author\":{\"contestId\":105020,\"members\":[{\"handle\":\"pastel\"},{\"handle\":\"eu_caue\"},{\"handle\":\"PMiguelez\"}],\"participantType\":\"VIRTUAL\",\"teamId\":156328,\"teamName\":\"a gente tem muito tempo\",\"ghost\":false,\"startTimeSeconds\":1712922240},\"programmingLanguage\":\"C++17 (GCC 7-32)\",\"verdict\":\"TIME_LIMIT_EXCEEDED\",\"testset\":\"TESTS\",\"passedTestCount\":4,\"timeConsumedMillis\":1000,\"memoryConsumedBytes\":4096000}]"

val mockGymSubmissions : List<CFSubmissionCompact> = listOf(
    CFSubmissionCompact(contestId=105020, creationTimeSeconds = 1712939730, problem=CFProblemCompact(index="L", name="Black and White Tree", listOf<String>()), verdict="OK"),
    CFSubmissionCompact(contestId=105020, creationTimeSeconds = 1712938759, problem=CFProblemCompact(index="D", name="Beautiful decrease", listOf<String>()), verdict="WRONG_ANSWER"),
    CFSubmissionCompact(contestId=105020, creationTimeSeconds = 1712938600, problem=CFProblemCompact(index="D", name="Beautiful decrease", listOf<String>()), verdict="TIME_LIMIT_EXCEEDED")
)

class SubmissionsRequestsTest {
    val requester = SubmissionRequests()
    @Test
    fun testRoundSubmissionsFromJSON() {
        val submissions = requester.contestSubmissionsFromJSON(mockRoundJSON)
        assertEquals(mockRoundSubmissions, submissions)
    }
    @Test
    fun testGymSubmissionsFromJSON() {
        val submissions = requester.contestSubmissionsFromJSON(mockGymJSON)
        assertEquals(mockGymSubmissions, submissions)
    }
}

const val mockContestStandingJSON =
    "{\"contest\":{\"id\":1951,\"name\":\"Codeforces Global Round 25\",\"type\":\"CF\",\"phase\":\"FINISHED\",\"frozen\":false,\"durationSeconds\":10800,\"startTimeSeconds\":1712414100,\"relativeTimeSeconds\":4509961},\"problems\":[{\"contestId\":1951,\"index\":\"A\",\"name\":\"Dual Trigger\",\"type\":\"PROGRAMMING\",\"points\":500.0,\"rating\":900,\"tags\":[\"constructive algorithms\",\"greedy\",\"math\"]},{\"contestId\":1951,\"index\":\"B\",\"name\":\"Battle Cows\",\"type\":\"PROGRAMMING\",\"points\":1000.0,\"rating\":1200,\"tags\":[\"binary search\",\"data structures\",\"greedy\"]},{\"contestId\":1951,\"index\":\"C\",\"name\":\"Ticket Hoarding\",\"type\":\"PROGRAMMING\",\"points\":1500.0,\"rating\":1400,\"tags\":[\"greedy\",\"math\",\"sortings\"]},{\"contestId\":1951,\"index\":\"D\",\"name\":\"Buying Jewels\",\"type\":\"PROGRAMMING\",\"points\":1750.0,\"rating\":2000,\"tags\":[\"constructive algorithms\",\"greedy\",\"math\"]},{\"contestId\":1951,\"index\":\"E\",\"name\":\"No Palindromes\",\"type\":\"PROGRAMMING\",\"points\":2250.0,\"rating\":2000,\"tags\":[\"brute force\",\"constructive algorithms\",\"divide and conquer\",\"greedy\",\"hashing\",\"implementation\",\"math\",\"strings\"]},{\"contestId\":1951,\"index\":\"F\",\"name\":\"Inversion Composition\",\"type\":\"PROGRAMMING\",\"points\":2750.0,\"rating\":2500,\"tags\":[\"constructive algorithms\",\"data structures\",\"greedy\"]},{\"contestId\":1951,\"index\":\"G\",\"name\":\"Clacking Balls\",\"type\":\"PROGRAMMING\",\"points\":3250.0,\"rating\":3100,\"tags\":[\"combinatorics\",\"math\",\"probabilities\"]},{\"contestId\":1951,\"index\":\"H\",\"name\":\"Thanos Snap\",\"type\":\"PROGRAMMING\",\"points\":3500.0,\"rating\":3200,\"tags\":[\"binary search\",\"dp\",\"games\",\"greedy\",\"trees\"]},{\"contestId\":1951,\"index\":\"I\",\"name\":\"Growing Trees\",\"type\":\"PROGRAMMING\",\"points\":4000.0,\"rating\":3200,\"tags\":[\"binary search\",\"constructive algorithms\",\"flows\",\"graphs\",\"greedy\"]}],\"rows\":[{\"party\":{\"contestId\":1951,\"members\":[{\"handle\":\"Geothermal\"}],\"participantType\":\"CONTESTANT\",\"ghost\":false,\"room\":522,\"startTimeSeconds\":1712414100},\"rank\":1,\"points\":13552.0,\"penalty\":0,\"successfulHackCount\":0,\"unsuccessfulHackCount\":0,\"problemResults\":[{\"points\":446.0,\"rejectedAttemptCount\":1,\"type\":\"FINAL\",\"bestSubmissionTimeSeconds\":185},{\"points\":924.0,\"rejectedAttemptCount\":1,\"type\":\"FINAL\",\"bestSubmissionTimeSeconds\":618},{\"points\":1440.0,\"rejectedAttemptCount\":0,\"type\":\"FINAL\",\"bestSubmissionTimeSeconds\":915},{\"points\":1620.0,\"rejectedAttemptCount\":0,\"type\":\"FINAL\",\"bestSubmissionTimeSeconds\":1725},{\"points\":1938.0,\"rejectedAttemptCount\":0,\"type\":\"FINAL\",\"bestSubmissionTimeSeconds\":3161},{\"points\":2068.0,\"rejectedAttemptCount\":0,\"type\":\"FINAL\",\"bestSubmissionTimeSeconds\":5587},{\"points\":2661.0,\"rejectedAttemptCount\":0,\"type\":\"FINAL\",\"bestSubmissionTimeSeconds\":4087},{\"points\":2455.0,\"rejectedAttemptCount\":0,\"type\":\"FINAL\",\"bestSubmissionTimeSeconds\":6725},{\"points\":0.0,\"rejectedAttemptCount\":0,\"type\":\"FINAL\"}]}]}"

val mockContest : CFContestCompact =
    CFContestCompact(
        id =1951,
        name="Codeforces Global Round 25",
        durationSeconds = 10800
    )

class ContestRequestsTest {
    val requester = ContestRequests()
    @Test
    fun testRoundSubmissionsFromJSON() {
        val contest = requester.contestFromJSON(mockContestStandingJSON)
        assertEquals(mockContest, contest)
    }
}