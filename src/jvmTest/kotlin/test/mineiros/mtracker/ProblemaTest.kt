import kotlinx.coroutines.runBlocking
import mineiros.mtracker.models.Problema
import mineiros.mtracker.models.db.DatabaseSingleton.createHikariDataSource
import mineiros.mtracker.models.db.ProblemaNome
import mineiros.mtracker.models.db.ProblemaTag
import mineiros.mtracker.models.db.ProblemasInfoService
import mineiros.mtracker.models.getNome
import mineiros.mtracker.models.getTags
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.Assert.assertEquals
import org.junit.Test
import kotlin.test.assertContains

class ProblemaTest {
    val problema = Problema("500")
    @Test
    fun testNome() {
        assertEquals("500", problema.id)
    }
}

fun initCleanProblemaMethods() {
    val driverClassName = "org.h2.Driver"
    val jdbcURL = "jdbc:h2:file:./build/db/test"
    val database = Database.connect(createHikariDataSource(jdbcURL, driverClassName))
    transaction(database){
        SchemaUtils.create(ProblemaNome, ProblemaTag)
        ProblemaNome.deleteAll()
        ProblemaTag.deleteAll()
    }
}

class ProblemaMethodsTest {
    val problema1 = Problema(id="102030")
    val nome1 = "Um Problema Muito Legal"
    val tags1 = listOf("implementação", "matemática", "muitos grafos")

    val problema2 = Problema(id="304050")
    val nome2 = "Um Problema Quase Tão Legal"
    val tags2 = listOf("mais grafos", "greedy", "implementação")

    init {
        initCleanProblemaMethods()
        runBlocking {
            ProblemasInfoService().addProblemaNome(problema1.id, nome1)
            for(tag in tags1)
                ProblemasInfoService().addProblemaTag(problema1.id, tag)

            ProblemasInfoService().addProblemaNome(problema2.id, nome2)
            for(tag in tags2)
                ProblemasInfoService().addProblemaTag(problema2.id, tag)
        }
    }
    @Test
    fun testGetNome() = runBlocking {
        assertEquals(nome1, problema1.getNome())
        assertEquals(nome2, problema2.getNome())
    }
    @Test
    fun testGetTags() = runBlocking {
        val tagsFor1 = problema1.getTags()
        val tagsFor2 = problema2.getTags()

        assertEquals(tags1.size, tagsFor1.size)
        for (tag in tagsFor1)
            assertContains(tags1, tag)

        assertEquals(tags2.size, tagsFor2.size)
        for (tag in tagsFor2)
            assertContains(tags2, tag)

    }
}