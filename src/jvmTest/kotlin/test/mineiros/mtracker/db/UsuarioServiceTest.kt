package test.mineiros.mtracker.db

import kotlinx.coroutines.runBlocking
import mineiros.mtracker.models.Usuario
import mineiros.mtracker.models.db.DatabaseSingleton.createHikariDataSource
import mineiros.mtracker.models.db.UsuarioService
import mineiros.mtracker.models.db.Usuarios
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.Assert.*
import org.junit.Test

fun initCleanUsuario() {
    val driverClassName = "org.h2.Driver"
    val jdbcURL = "jdbc:h2:file:./build/db/test"
    val database = Database.connect(createHikariDataSource(jdbcURL, driverClassName))
    transaction(database){
        SchemaUtils.create(Usuarios)
        Usuarios.deleteAll()
    }
}

class UsuarioServiceTest {
    val service = UsuarioService()
    val usuarioPaulo = Usuario("paulo", "rmm")
    val usuarioKelly = Usuario("kelly", "bd")

    init {
        initCleanUsuario()
    }

    @Test
    fun testAllUsuarios() = runBlocking {
        assertEquals(listOf<Usuario>(), service.allUsuarios())
    }

    @Test
    fun testAddNewUsuarioFirst() = runBlocking {
        val ret = service.addNewUsuario("paulo", "rmm")
        assertEquals(ret, usuarioPaulo)
        assertEquals(listOf<Usuario>(usuarioPaulo), service.allUsuarios())
    }

    @Test
    fun testUsuario() = runBlocking {
        service.addNewUsuario("paulo", "rmm")
        val ret = service.usuario("paulo")
        assertEquals(ret, usuarioPaulo)
    }

    @Test
    fun testAddNewUsuarioSecond() = runBlocking {
        service.addNewUsuario("paulo", "rmm")
        val ret = service.addNewUsuario("kelly", "bd")
        assertEquals(ret, usuarioKelly)
    }

    @Test
    fun testDeleteUsuario() = runBlocking {
        service.addNewUsuario("paulo", "rmm")
        assertNotNull(service.usuario("paulo"))
        service.deleteUsuario("paulo")
        assertNull(service.usuario("paulo"))
    }
}