package test.mineiros.mtracker.db

import kotlinx.coroutines.runBlocking
import mineiros.mtracker.models.Contest
import mineiros.mtracker.models.Problema
import mineiros.mtracker.models.db.*
import mineiros.mtracker.models.db.DatabaseSingleton.createHikariDataSource
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.Assert.assertFalse
import org.junit.Test
import test.mineiros.mtracker.codeforces.*
import kotlin.test.*

fun initCleanContestsProblemas() {
    val driverClassName = "org.h2.Driver"
    val jdbcURL = "jdbc:h2:file:./build/db/test"
    val database = Database.connect(createHikariDataSource(jdbcURL, driverClassName))
    transaction(database){
        SchemaUtils.create(Usuarios, ContestsProblemas)
        Usuarios.deleteAll()
        ContestsProblemas.deleteAll()
    }
}

class ContestsProblemasServiceTest {

    val service = ContestsProblemasService()
    val problema1 = Problema("1")
    val problema2 = Problema("2")
    val problema3 = Problema("3")
    val problema4 = Problema("4")
    val nomePaulo = "paulo"

    init {
        initCleanContestsProblemas()
        runBlocking {
            UsuarioService().addNewUsuario(nomePaulo, "rmm")
        }
    }

    val contest1 = Contest(1, listOf(problema1))
    val contest2 = Contest(2, listOf(problema2, problema3, problema4))
    @Test
    fun testAllContests() = runBlocking {
        assertContentEquals(listOf(), service.allContests(nomePaulo))
    }
    @Test
    fun testAddNewContest() = runBlocking {
        service.addNewContest(nomePaulo, contest1)
        assertContentEquals(listOf(contest1), service.allContests(nomePaulo))
    }
    @Test
    fun testAddNewContestMuitos() = runBlocking {
        service.addNewContest(nomePaulo, contest1)
        service.addNewContest(nomePaulo, contest2)
        assertContains(service.allContests(nomePaulo), contest1)
        assertContains(service.allContests(nomePaulo), contest2)
    }
    @Test
    fun testProblemasFromContestId() = runBlocking {
        service.addNewContest(nomePaulo, contest1)
        val problemas = service.problemasFromUserContest(nomePaulo, 1)
        assertContentEquals(listOf(problema1), problemas)
    }
    @Test
    fun testProblemasFromContestIdMuitos() = runBlocking {
        service.addNewContest(nomePaulo, contest2)
        val problemas = service.problemasFromUserContest(nomePaulo, 2)
        assertContains(problemas, problema2)
        assertContains(problemas, problema3)
        assertContains(problemas, problema4)
    }
    @Test
    fun testDeleteContest() = runBlocking {
        service.addNewContest(nomePaulo, contest1)
        service.addNewContest(nomePaulo, contest2)

        val ret1 = service.deleteContest(nomePaulo, contest1)
        assert(ret1)

        val ret1again = service.deleteContest(nomePaulo, contest1)
        assertFalse(ret1again)
        assertContentEquals(listOf(contest2), service.allContests(nomePaulo))
    }
    @Test
    fun testDeleteContestMuitos() = runBlocking {
        service.addNewContest(nomePaulo, contest1)
        service.addNewContest(nomePaulo, contest2)

        val ret1 = service.deleteContest(nomePaulo, contest2)
        assert(ret1)
        assertContentEquals(listOf(contest1), service.allContests(nomePaulo))

        val ret2 = service.deleteContest(nomePaulo, contest1)
        assert(ret2)
        assertContentEquals(listOf<Contest>(), service.allContests(nomePaulo))
    }
    @Test
    fun testAddContestAgain() = runBlocking {
        service.addNewContest(nomePaulo, contest1)
        service.addNewContest(nomePaulo, contest1)
        assertContentEquals(listOf(contest1), service.allContests(nomePaulo))
    }
    @Test
    fun testAddFromCFSubmissionGym() = runBlocking {
        service.addFromCFSubmission(nomePaulo, mockGymSubmissions)
        val contest = Contest(
            id=105020,
            problemas = listOf(
                Problema(
                    id="105020L"
                )
            )
        )
        val contests = service.allContests(nomePaulo)
        assertContentEquals(listOf(contest), contests)
    }
    @Test
    fun testAddFromCFSubmissionRound() = runBlocking {
        service.addFromCFSubmission(nomePaulo, mockRoundSubmissions)
        val contests = service.allContests(nomePaulo)
        assertEquals(1,contests.size)
        val contest1 = contests.single()
        assertEquals(2,contest1.problemas.size)
        assertContains(contest1.problemas, Problema(id="1951E"))
        assertContains(contest1.problemas, Problema(id="1951D"))
    }
}