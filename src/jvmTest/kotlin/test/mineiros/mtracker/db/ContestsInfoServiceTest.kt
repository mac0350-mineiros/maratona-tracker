package test.mineiros.mtracker.db

import kotlinx.coroutines.runBlocking
import mineiros.mtracker.models.db.*
import mineiros.mtracker.models.db.DatabaseSingleton.createHikariDataSource
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.Test
import kotlin.test.assertEquals

fun initCleanContestsInfo() {
    val driverClassName = "org.h2.Driver"
    val jdbcURL = "jdbc:h2:file:./build/db/test"
    val database = Database.connect(createHikariDataSource(jdbcURL, driverClassName))
    transaction(database){
        SchemaUtils.create(ContestsInfo)
        ContestsInfo.deleteAll()
    }
}

class ContestsInfoServiceTest {
    val service = ContestsInfoService()
    val id1 = 1951
    val id2 = 20000
    val nome1 = "Codeforces Some Round"
    val nome2 = "A Very Good ICPC Contest From A Very Interesting Region Probably in Europe Having A Unnecessarily Large Name"
    val duracao = 10000

    init {
        initCleanContestsInfo()
    }

    @Test
    fun testUmContest() = runBlocking {
        service.addContest(id1, nome1, duracao)
        assertEquals(nome1, service.nome(id1))
        assertEquals(duracao, service.duracaoS(id1))
    }
    @Test
    fun testAdicionaDuasVeze() = runBlocking {
        service.addContest(id1, nome1, duracao)
        service.addContest(id1, nome1, duracao)
        assertEquals(nome1, service.nome(id1))
        assertEquals(duracao, service.duracaoS(id1))
    }
    @Test
    fun testMuitosContests() = runBlocking {
        service.addContest(id1, nome1, duracao)
        assertEquals(nome1, service.nome(id1))
        assertEquals(duracao, service.duracaoS(id1))

        service.addContest(id2, nome2, duracao+1000)
        assertEquals(nome2, service.nome(id2))
        assertEquals(duracao+1000, service.duracaoS(id2))
    }
}