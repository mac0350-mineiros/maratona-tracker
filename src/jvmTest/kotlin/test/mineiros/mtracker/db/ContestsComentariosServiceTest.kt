package test.mineiros.mtracker.db

import kotlinx.coroutines.runBlocking
import mineiros.mtracker.models.*
import mineiros.mtracker.models.db.*
import mineiros.mtracker.models.db.DatabaseSingleton.createHikariDataSource
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.Test
import kotlin.test.assertEquals

fun initCleanContestsComentarios() {
    val driverClassName = "org.h2.Driver"
    val jdbcURL = "jdbc:h2:file:./build/db/test"
    val database = Database.connect(createHikariDataSource(jdbcURL, driverClassName))
    transaction(database){
        SchemaUtils.create(Usuarios, ContestsComentarios, ContestsInfo)
        Usuarios.deleteAll()
        ContestsComentarios.deleteAll()
    }
}

class ContestsComentariosServiceTest {

    val service = ContestsComentariosService()
    val nomePaulo = "paulo"
    val contest = Contest(2024, listOf<Problema>())
    val inicio = 1212345678

    init {
        initCleanContestsComentarios()
        runBlocking {
            UsuarioService().addNewUsuario(nomePaulo, "rmm")
            service.addComentarioInicial(nomePaulo, contest, inicio)
            ContestsInfoService().addContest(contest.id, "um contest", 1000)
        }
    }

    @Test
    fun testComentarioInicial() = runBlocking {
        assertEquals(comentarioInicial, service.comentario(nomePaulo, contest))
    }
    @Test
    fun testSetComentario() = runBlocking {
        val novoComentario = "Um novo comentário!"
        service.setComentario(nomePaulo, contest, novoComentario)
        assertEquals(novoComentario, service.comentario(nomePaulo, contest))
    }
    @Test
    fun testResetComentario() = runBlocking {
        val novoComentario = "Um novo comentário!"
        val outroComentario = "Mais um belo comentário!"
        service.setComentario(nomePaulo, contest, novoComentario)
        service.setComentario(nomePaulo, contest, outroComentario)
        assertEquals(outroComentario, service.comentario(nomePaulo, contest))
    }
    @Test
    fun testComentarioMuitoGrande() = runBlocking {
        val stringGrande = "eu tu ele ".repeat(1000)
        assert(stringGrande.length > MAX_COMENTARIO)
        service.setComentario(nomePaulo, contest, stringGrande)
        assertEquals(stringGrande.take(MAX_COMENTARIO), service.comentario(nomePaulo, contest))
    }
    @Test
    fun testGetInicio() = runBlocking {
        assertEquals(inicio, service.tempoS(nomePaulo, contest))
    }
}