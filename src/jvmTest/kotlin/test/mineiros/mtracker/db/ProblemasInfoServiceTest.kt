package test.mineiros.mtracker.db

import kotlinx.coroutines.runBlocking
import mineiros.mtracker.models.Problema
import mineiros.mtracker.models.codeforces.CFProblemCompact
import mineiros.mtracker.models.codeforces.CFSubmissionCompact
import mineiros.mtracker.models.db.*
import mineiros.mtracker.models.db.DatabaseSingleton.createHikariDataSource
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.junit.Test
import kotlin.test.assertContains
import kotlin.test.assertEquals

fun initCleanProblemasInfo() {
    val driverClassName = "org.h2.Driver"
    val jdbcURL = "jdbc:h2:file:./build/db/test"
    val database = Database.connect(createHikariDataSource(jdbcURL, driverClassName))
    transaction(database){
        SchemaUtils.create(ProblemaNome, ProblemaTag)
        ProblemaNome.deleteAll()
        ProblemaTag.deleteAll()
    }
}

class ProblemasInfoServiceTest {
    val service = ProblemasInfoService()

    val problema1 = Problema(id="1040A")
    val nome1 = "Um Problema Muito Legal"
    val tags1 = listOf("implementação", "matemática", "muitos grafos")

    val problema2 = Problema(id="_numero_Y")
    val nome2 = "Um Problema Quase Tão Legal"
    val tags2 = listOf("mais grafos", "greedy", "implementação")

    init {
        initCleanProblemasInfo()
    }

    @Test
    fun testNome() = runBlocking {
        service.addProblemaNome(problema1.id, nome1)
        assertEquals(nome1, service.nome(problema1.id))
    }
    @Test
    fun testTag() = runBlocking {
        for (tag in tags1)
            service.addProblemaTag(problema1.id, tag)
        val tags = service.tags(problema1.id)
        assertEquals(tags1.size, tags.size)
        for (tag in tags1)
            assertContains(tags, tag)
    }
    @Test
    fun testAddTagList() = runBlocking {
        service.addProblemaTag(problema1.id, tags1)
        val tags = service.tags(problema1.id)
        assertEquals(tags1.size, tags.size)
        for (tag in tags1)
            assertContains(tags, tag)
    }
    @Test
    fun testNomeMuitos() = runBlocking {
        service.addProblemaNome(problema1.id, nome1)
        service.addProblemaNome(problema2.id, nome2)
        assertEquals(nome1, service.nome(problema1.id))
        assertEquals(nome2, service.nome(problema2.id))
    }
    @Test
    fun testTagArray() = runBlocking {
        for (tag in tags1)
            service.addProblemaTag(problema1.id, tag)
        for (tag in tags2)
            service.addProblemaTag(problema2.id, tag)

        val tagsFor1 = service.tags(problema1.id)
        assertEquals(tags1.size, tagsFor1.size)
        for (tag in tags1)
            assertContains(tagsFor1, tag)

        val tagsFor2 = service.tags(problema2.id)
        assertEquals(tags2.size, tagsFor2.size)
        for (tag in tags2)
            assertContains(tagsFor2, tag)
    }
    @Test
    fun testInfoFromCFSubmission() = runBlocking {
        val cfSubmission = CFSubmissionCompact(
            contestId=105020,
            creationTimeSeconds = 1712939730,
            problem= CFProblemCompact(
                index="L",
                name="Black and White Tree",
                listOf<String>()
            ),
            verdict="OK"
        )
        service.addInfoFromCFSubmission(cfSubmission)
        assertEquals(cfSubmission.problem.name,service.nome("105020L"))
        assertEquals(listOf<String>(),service.tags("105020L"))
    }
}