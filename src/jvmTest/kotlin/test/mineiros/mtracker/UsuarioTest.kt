import kotlinx.coroutines.runBlocking
import mineiros.mtracker.models.*
import mineiros.mtracker.models.db.*
import mineiros.mtracker.models.db.DatabaseSingleton.createHikariDataSource
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.*
import org.junit.Test
import org.junit.Assert.*
import kotlin.test.assertContains

class UsuarioTest {
    val user = Usuario("Paulo", "rmm")
    @Test
    fun testNome() {
        assertEquals("Paulo", user.nome)
    }
    @Test
    fun testHandle() {
        assertEquals("rmm", user.handle)
    }
}

fun initCleanUsuarioMethods() {
    val driverClassName = "org.h2.Driver"
    val jdbcURL = "jdbc:h2:file:./build/db/test"
    val database = Database.connect(createHikariDataSource(jdbcURL, driverClassName))
    transaction(database){
        SchemaUtils.create(Usuarios, ContestsProblemas, ContestsComentarios, ProblemaTag, ProblemaNome)
        Usuarios.deleteAll()
        ContestsProblemas.deleteAll()
        ContestsComentarios.deleteAll()
        ProblemaTag.deleteAll()
        ProblemaNome.deleteAll()
    }
}

class UsuarioMethodsTest {
    val user = Usuario("karademamao", "pastel")
    val comentario = "Um gracioso comentário"

    val problema1 = Problema(id="404040")
    val nome1 = "Problema 1"
    val tags1 = listOf("matemática", "implementação", "greedy")
    val problema2 = Problema(id="50")
    val nome2 = "Problema 2"
    val tags2 = listOf("matemática", "greedy")
    val problema3 = Problema(id="12")
    val nome3 = "Problema 3"
    val tags3 = listOf("grafos", "matemática")

    val tempo = 100
    val contest = Contest(id=1234, listOf(problema1, problema2, problema3))

    init {
        initCleanUsuarioMethods()
        runBlocking {
            UsuarioService().addNewUsuario(user.nome, user.handle)
            ContestsProblemasService().addNewContest(user.nome, contest)
            ContestsComentariosService().addComentarioInicial(user.nome, contest, tempo)

            ProblemasInfoService().addProblemaNome(problema1.id, nome1)
            ProblemasInfoService().addProblemaTag(problema1.id, tags1)
            ProblemasInfoService().addProblemaNome(problema2.id, nome2)
            ProblemasInfoService().addProblemaTag(problema2.id, tags2)
            ProblemasInfoService().addProblemaNome(problema3.id, nome3)
            ProblemasInfoService().addProblemaTag(problema3.id, tags3)
        }
    }
    @Test
    fun testGetContests() = runBlocking {
        val userProblemas : List<Problema> = user.getContests().single().problemas
        assertEquals(userProblemas.size, contest.problemas.size)
        for (problema in userProblemas)
            assertContains(contest.problemas, problema)
    }
    @Test
    fun testComentario() = runBlocking {
        user.setComentario(contest, comentario)
        assertEquals(comentario, user.getComentario(contest))
    }
    @Test
    fun testTempoS() = runBlocking {
        assertEquals(tempo, user.getTempoS(contest))
    }
    @Test
    fun testComentarioMudou() = runBlocking {
        user.setComentario(contest, comentario)
        assertEquals(comentario, user.getComentario(contest))
        val novoComentario = comentario + "!"
        user.setComentario(contest, novoComentario)
        assertEquals(novoComentario, user.getComentario(contest))
    }
    @Test
    fun testGetTags() = runBlocking {
        val frequencyOfTags = user.getTags()
        assertEquals(4,frequencyOfTags.size)
        assertEquals(1,frequencyOfTags["implementação"])
        assertEquals(1,frequencyOfTags["grafos"])
        assertEquals(2,frequencyOfTags["greedy"])
        assertEquals(3,frequencyOfTags["matemática"])
    }
}