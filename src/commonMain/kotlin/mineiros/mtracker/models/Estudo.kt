package mineiros.mtracker.models

import kotlinx.serialization.Serializable

@Serializable
data class Estudo(
    val user: Usuario,
    val tempo: Int
)

/*@Serializable
data class StudyAdd(
    val handle: String,
)*/
