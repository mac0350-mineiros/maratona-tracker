package mineiros.mtracker.models

import kotlinx.serialization.Serializable

@Serializable
data class Usuario(
    val nome: String,
    val handle: String
)
