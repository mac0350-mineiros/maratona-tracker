package mineiros.mtracker.models

import kotlinx.serialization.Serializable

@Serializable
data class Comentario (
    var body: String?,
)