package mineiros.mtracker.models

import kotlinx.serialization.Serializable

@Serializable
data class Problema(
    val id: String
)

@Serializable
data class ProblemaShow(
    val id: String,
    val nome: String
)