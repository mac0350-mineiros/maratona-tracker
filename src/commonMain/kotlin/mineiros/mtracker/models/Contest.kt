package mineiros.mtracker.models

import kotlinx.serialization.Serializable

@Serializable
data class Contest(
    val id : Int,
    val problemas: List<Problema>
)

@Serializable
data class ContestAdd(
    val id: String,
)

@Serializable
data class ContestShow(
    val id: Int,
    val nome: String?,
    val tempo: Int?,
    val data: String?,
    val problemas: List<ProblemaShow>
)