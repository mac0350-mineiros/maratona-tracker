package mineiros.mtracker

import io.kvision.chart.*
import io.kvision.html.div
import io.kvision.panel.SimplePanel
import io.kvision.panel.gridPanel
import kotlinx.serialization.json.Json
import mineiros.mtracker.models.ContestShow

object Charts : SimplePanel() {
    private var chartPanel = gridPanel(templateColumns = "47% 6% 47%")
    private lateinit var barChart : Chart
    private lateinit var lineChart : Chart
    private var space = div()

    private lateinit var dataBarChart : MutableList<Int>
    private lateinit var tagsBarChart : MutableList<String>
    private lateinit var dataLineChart : MutableList<Int>
    private lateinit var datesLineChart : MutableList<String>

    fun reload() {
        chartPanel.removeAll()
        update()
        chartPanel.add(barChart)
        chartPanel.add(space)
        chartPanel.add(lineChart)
    }

    private fun update() {
        val json = Json { ignoreUnknownKeys = true }

        // gráfico de tags mais frequentes
        dataBarChart = mutableListOf()
        tagsBarChart = mutableListOf()

        tags.forEach {
            dataBarChart.add(it.value)
            tagsBarChart.add(it.key)
        }

        barChart = chart(
            Configuration(
                ChartType.BAR,
                listOf(
                    DataSets(
                        data = dataBarChart,
                        indexAxis = "y"
                    )
                ),
                tagsBarChart,
                ChartOptions(
                    plugins = PluginsOptions(
                        legend = LegendOptions(display = false),
                        title = TitleOptions(text = listOf("Tags mais frequentes"), display = true)
                    )
                )
            )
        )

        // gráfico de frequência
        dataLineChart = mutableListOf()
        datesLineChart = mutableListOf()

        val mapDates: MutableMap<String, Int> = mutableMapOf()

        contestList.forEach {
            if(it.data != null) {
                if(mapDates.containsKey(it.data))
                    mapDates[it.data] = mapDates[it.data]!! + it.problemas.size
                else
                    mapDates[it.data] = it.problemas.size
            }
        }

        for(i in mapDates.toList().sortedBy {
            (key, _) -> "${key.substring(6)}/${key.substring(3, 5)}/${key.substring(0, 2)}"
        }.toMap()){
            dataLineChart.add(i.value)
            datesLineChart.add(i.key)
        }

        lineChart = chart(
            Configuration(
                ChartType.LINE,
                listOf(
                    DataSets(
                        data = dataLineChart
                    )
                ),
                datesLineChart,
                ChartOptions(
                    plugins = PluginsOptions(
                        legend = LegendOptions(display = false),
                        title = TitleOptions(text = listOf("Frequência"), display = true)
                    ),
                    scales = mapOf("y" to ChartScales(beginAtZero = true))
                )
            )
        )
    }
}