package mineiros.mtracker

import io.kvision.form.formPanel
import io.kvision.form.text.Text
import io.kvision.html.*
import io.kvision.panel.VPanel
import io.kvision.panel.fieldsetPanel
import io.kvision.panel.vPanel
import io.kvision.tabulator.ColumnDefinition
import io.kvision.tabulator.Layout
import io.kvision.tabulator.TabulatorOptions
import io.kvision.tabulator.tabulator
import io.kvision.toast.Toast
import io.kvision.utils.px
import kotlinx.coroutines.launch
import mineiros.mtracker.models.Comentario
import mineiros.mtracker.models.ContestShow

class ContestPanel(contest: ContestShow) : VPanel() {
    private val commentForm = formPanel<Comentario> {
        add(Comentario::body, Text())
    }

    init {
        val url = "https://codeforces.com/" + if(contest.id >= 100001) { "gym/" } else { "contest/" } + "${contest.id}"

        vPanel(spacing = 12) {
            fieldsetPanel("ID") {
                paddingTop = 10.px
                paddingBottom = 10.px
                div { link("${contest.id}", url) }
            }

            fieldsetPanel("Tempo") {
                paddingTop = 10.px
                paddingBottom = 10.px
                div("${contest.tempo?.div(60)} minutos")
            }

            fieldsetPanel("Data") {
                paddingTop = 10.px
                paddingBottom = 10.px
                div("${contest.data}")
            }

            fieldsetPanel("Problemas resolvidos") {
                paddingTop = 10.px
                paddingBottom = 10.px

                tabulator(
                    contest.problemas, options = TabulatorOptions(
                        layout = Layout.FITCOLUMNS,
                        columns = listOf(
                            ColumnDefinition("ID", "id", headerSort = false, widthGrow = 1, cellClick = {
                                _, cell -> redirect("$url/problem/${cell.getValue().toString().last()}")
                            }),
                            ColumnDefinition("Nome", "nome", headerSort = false, widthGrow = 9),
                        )
                    )
                )
            }

            fieldsetPanel("Comentário") {
                paddingTop = 10.px
                paddingBottom = 10.px

                add(commentForm)
                commentForm.setData(Comentario(comments[contest.id]?:""))

                add(Button("Salvar", style = ButtonStyle.SUCCESS){
                    onClick {
                        val data = commentForm.getData()
                        AppScope.launch {
                            if(post("setComentario", mapOf("handle" to handle, "id" to contest.id.toString(),
                                "body" to (data.body ?: ""))))
                                Toast.success("Comentário atualizado.")
                            else
                                Toast.danger("Erro ao atualizar comentário.")
                        }
                    }
                })
            }

            div(className = "d-flex align-items-right justify-content-end") {
                button("", "fa-regular fa-trash-can", ButtonStyle.DANGER).onClick {
                    // TODO
                }
            }
        }
    }
}