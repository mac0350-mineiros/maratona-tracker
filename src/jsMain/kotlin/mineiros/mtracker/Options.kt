package mineiros.mtracker

import io.kvision.core.Container
import io.kvision.core.JustifyContent
import io.kvision.dropdown.dropDown
import io.kvision.form.formPanel
import io.kvision.form.text.Text
import io.kvision.html.*
import io.kvision.modal.Modal
import io.kvision.panel.hPanel
import io.kvision.panel.vPanel
import io.kvision.toast.Toast
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import mineiros.mtracker.models.ContestAdd
//import mineiros.mtracker.models.StudyAdd

fun Container.options() {
    val contestFormPanel = formPanel<ContestAdd> {
        add(ContestAdd::id, Text(label = "ID"), required = true)
    }

    val contestModal = Modal("Registrar contest")
    contestModal.add(contestFormPanel)
    contestModal.add(Button("Salvar", style = ButtonStyle.SUCCESS){
        onClick {
            if(contestFormPanel.validate()) {
                val data = contestFormPanel.getData()
                AppScope.launch {
                    if(post("addContest", mapOf("handle" to handle, "id" to data.id)))
                        Toast.success("Contest adicionado.")
                    else
                        Toast.danger("Erro ao adicionar contest.")
                    delay(1000)
                    refreshFront()
                }
                contestModal.hide()
            }
        }
    })

    /*val studyFormPanel = formPanel<StudyAdd> {
        add(StudyAdd::handle, Text(label = "Handle no Codeforces"), required = true)
    }*/

    val studyModal = Modal("Registrar estudo")
    studyModal.add(span("Indisponível no momento."))
    /*studyModal.add(studyFormPanel)
    studyModal.add(Button("Salvar", style = ButtonStyle.SUCCESS){
        onClick {
            if(studyFormPanel.validate()) {
                val data = studyFormPanel.getData()
                AppScope.launch {
                    // TODO: post para estudos
                }
                studyModal.hide()
            }
        }
    })*/

    section {
        hPanel(justify = JustifyContent.START, spacing = 20, className = "container") {
            dropDown(
                text = "Adicionar",
                icon = "fa-regular fa-plus fa-fw",
                style = ButtonStyle.WARNING
            ){
                vPanel {
                    button("Contest", style = ButtonStyle.LIGHT).onClick {
                        contestModal.show()
                    }
                    button("Estudo", style = ButtonStyle.LIGHT).onClick {
                        studyModal.show()
                    }
                }
            }

            button("Relatório", "fa-regular fa-file-lines fa-fw", ButtonStyle.WARNING).onClick {
                redirect("http://localhost:8080/relatorio?handle=$handle")
            }
        }
    }
}
