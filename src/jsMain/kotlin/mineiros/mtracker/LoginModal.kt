package mineiros.mtracker

import io.kvision.core.Container
import io.kvision.form.formPanel
import io.kvision.form.text.Text
import io.kvision.html.Button
import io.kvision.html.ButtonStyle
import io.kvision.html.section
import io.kvision.modal.Modal
import kotlinx.coroutines.launch
import kotlinx.serialization.Serializable

@Serializable
data class LoginClass (
    val handle : String
)

fun Container.loginModal() {
    val loginForm = formPanel<LoginClass> {
        add(LoginClass::handle, Text(label = "Handle no Codeforces"), required = true)
    }

    val loginModal = Modal("Login", closeButton = false, escape = false)
    loginModal.add(loginForm)
    loginModal.add(Button("Salvar", style = ButtonStyle.SUCCESS){
        onClick {
            if(loginForm.validate()) {
                handle = loginForm.getData().handle
                AppScope.launch { refreshFront() }
                loginModal.hide()
            }
        }
    })

    section {
        loginModal.show()
    }
}