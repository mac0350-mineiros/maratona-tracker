package mineiros.mtracker

import io.kvision.dropdown.dropDown
import io.kvision.html.*
import io.kvision.modal.Confirm
import io.kvision.panel.vPanel
import io.kvision.toast.Toast

object Topbar : Nav(className = "navbar bg-light") {
    private val barDiv : Div = div(className = "container") {
        div(className = "navbar-brand") {
            image("images/logo.svg", className = "navbar-item is-inline-block") {
                setAttribute("height", "60")
            }
        }
    }

    private var btnTopBar : Div = div(className = "navbar-end")

    init {
        barDiv.add(btnTopBar)
    }

    fun reload() {
        barDiv.remove(btnTopBar)

        btnTopBar = div(className = "navbar-end") {
            dropDown(
                text = "$handle ",
                icon = "fa-regular fa-user fa-fw",
                style = ButtonStyle.WARNING,
                className = "navbar-item"
            ){
                vPanel {
                    button("Apagar dados", style = ButtonStyle.LIGHT).onClick {
                        Confirm.show(
                            "Apagar dados",
                            "Você possui certeza que deseja apagar seus dados da plataforma? Esta operação é irreversível.",
                            yesTitle = "Sim",
                            noTitle = "Não"){
                            // TODO: invocar back-end e apagar dados da tabela
                            /*handle = ""
                            loginModal()
                            Toast.success("Dados apagados com sucesso.")*/
                            Toast.info("Operação indisponível.")
                        }
                    }
                    button("Sair", style = ButtonStyle.LIGHT).onClick {
                        handle = ""
                        loginModal()
                    }
                }
            }
        }

        barDiv.add(btnTopBar)
    }
}
