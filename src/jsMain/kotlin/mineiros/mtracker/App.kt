package mineiros.mtracker

import io.kvision.*
import io.kvision.panel.root
import io.kvision.panel.vPanel
import io.kvision.utils.auto
import io.kvision.utils.perc
import io.kvision.utils.px
import kotlinx.browser.window
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import mineiros.mtracker.models.ContestShow

val AppScope = CoroutineScope(window.asCoroutineDispatcher())

var handle: String = ""
var contestList: List<ContestShow> = listOf()
var tags: Map<String, Int> = mapOf()
var comments: MutableMap<Int, String> = mutableMapOf()

suspend fun refreshFront() { // recarrega todos os objetos que dependem de dados do back-end
    val content = get("contests", mapOf("handle" to handle))
    val json = Json { ignoreUnknownKeys = true }
    contestList = json.decodeFromString<List<ContestShow>>(content)

    val contentTags = get("tags", mapOf("handle" to handle))
    tags = json.decodeFromString<Map<String, Int>>(contentTags)

    contestList.forEach {
        comments[it.id] = get("comentario", mapOf("handle" to handle, "id" to it.id.toString()))
    }

    Topbar.reload()
    Hours.reload()
    Charts.reload()
    MainPanel.loadContestPanel()
    MainPanel.loadStudyPanel()
}

class App : Application() {
    override fun start() {
        root("app") {
            add(Topbar) // barra de título

            vPanel(spacing = 50) { // conteúdo em si da página
                width = 90.perc
                margin = 10.px
                marginLeft = auto
                marginRight = auto
                padding = 20.px

                add(Hours) // indicador do total de horas

                add(Charts) // gráficos

                options() // menu de opções para usuário

                add(MainPanel) // exibir contest + estudos
            }

            if(handle == "") loginModal() // tela de login
        }
    }
}

fun main() {
    startApplication(
        ::App,
        module.hot,
        BootstrapModule,
        BootstrapCssModule,
        FontAwesomeModule,
        ChartModule,
        TabulatorModule,
        TabulatorCssBootstrapModule,
        ToastifyModule,
        CoreModule
    )
}
