package mineiros.mtracker

import io.kvision.html.*
import io.kvision.panel.*
import io.kvision.utils.perc
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import mineiros.mtracker.models.Contest
import mineiros.mtracker.models.ContestShow

object MainPanel : VPanel() {
    private var tabPanel : TabPanel
    private var contestsDiv : Div
    private var studiesDiv : Div

    init {
        width = 100.perc

        contestsDiv = div(className = "accordion") { setAttribute("id", "accordionContestsPanel") }

        studiesDiv = div(className = "accordion") { setAttribute("id", "accordionStudyPanel") }

        tabPanel = tabPanel {
            tab("Contests", icon = "fa-regular fa-lightbulb fa-fw"){
                add(contestsDiv)
            }
            tab("Estudos", icon = "fa-regular fa-pen-to-square fa-fw"){
                add(studiesDiv)
            }
        }
    }

    private fun addToContestPanel(contest: ContestShow, nContests: Int) {
        contestsDiv.add(
            div(className = "accordion-item") {
                h2(className = "accordion-header") {
                    button(
                        text = "${contest.nome}",
                        style = ButtonStyle.WARNING, className = "accordion-button collapsed"
                    ) {
                        setAttribute("data-bs-toggle", "collapse")
                        setAttribute("data-bs-target", "#contest-collapse-$nContests")
                        setAttribute("aria-expanded", "false")
                        setAttribute("aria-controls", "contest-collapse-$nContests")
                    }
                }
                div(className = "collapse") {
                    setAttribute("id", "contest-collapse-$nContests")

                    div(className = "accordion-body") {
                        add(ContestPanel(contest))
                    }
                }
            }
        )
    }

    fun loadContestPanel() {
        contestsDiv.removeAll()

        var nContests = 0

        contestList.forEach {
            addToContestPanel(it, nContests)
            nContests++
        }
    }

    fun addToStudyPanel(nStudies: Int) {
        studiesDiv.add(
            div(className = "accordion-item") {
                h2(className = "accordion-header") {
                    button(
                        text = "Estudo #$nStudies",
                        style = ButtonStyle.WARNING, className = "accordion-button collapsed"
                    ) {
                        setAttribute("data-bs-toggle", "collapse")
                        setAttribute("data-bs-target", "#study-collapse-$nStudies")
                        setAttribute("aria-expanded", "false")
                        setAttribute("aria-controls", "study-collapse-$nStudies")
                    }
                }
                div(className = "collapse") {
                    setAttribute("id", "study-collapse-$nStudies")

                    div(className = "accordion-body") {
                        add(StudyPanel())
                    }
                }
            }
        )
    }

    fun loadStudyPanel() {
        studiesDiv.removeAll()

        // for(i in 1..n) // TODO: realizar requisição do BD
        //    addToStudyPanel()
    }
}
