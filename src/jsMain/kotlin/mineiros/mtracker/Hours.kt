package mineiros.mtracker

import io.kvision.html.Div
import io.kvision.html.h4

object Hours : Div(className = "h-100 d-flex align-items-center justify-content-center") {
    private var total = 0

    init {
        h4("Seu progresso total: -")
    }

    fun reload() {
        total = 0

        contestList.forEach {
            total += it.tempo ?: 0
        }

        this.removeAll()
        this.add(h4("Seu progresso total: ${total/3600} hora(s) e ${(total/60)%60} minuto(s)"))
    }
}