package mineiros.mtracker

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.js.*
import io.ktor.client.request.*
import io.ktor.http.*

private const val backendPort = 8080

suspend fun post(baseurl: String, map: Map<String, String>) : Boolean {
    var url = "http://localhost:$backendPort/$baseurl?"
    map.forEach { entry ->
        url = "$url${entry.key}=${entry.value}&"
    }
    if(url.last() == '&') url = url.dropLast(1)

    val client = HttpClient(Js)
    val response = client.post(url)
    client.close()

    if(response.status == HttpStatusCode.OK) return true
    else return false
}

suspend fun get(baseurl: String, map: Map<String, String>) : String {
    var url = "http://localhost:$backendPort/$baseurl?"
    map.forEach { entry ->
        url = "$url${entry.key}=${entry.value}&"
    }
    if(url.last() == '&') url = url.dropLast(1)

    val client = HttpClient(Js)
    val response = client.get(url)
    client.close()

    return if(response.status == HttpStatusCode.OK) response.body()
           else ""
}

fun redirect(url: String){
    js("window.open(url)")
}
