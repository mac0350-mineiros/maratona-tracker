package mineiros.mtracker.models

import kotlinx.coroutines.runBlocking
import mineiros.mtracker.models.db.ProblemasInfoService

fun Problema.getNome(): String? = runBlocking {
    ProblemasInfoService().nome(id)
}

fun Problema.getTags(): List<String> = runBlocking {
    ProblemasInfoService().tags(id)
}