package mineiros.mtracker.models

import kotlinx.coroutines.runBlocking
import mineiros.mtracker.models.codeforces.CFContestCompact
import mineiros.mtracker.models.codeforces.ContestRequests
import mineiros.mtracker.models.codeforces.SubmissionRequests
import mineiros.mtracker.models.db.ContestsComentariosService
import mineiros.mtracker.models.db.ContestsInfoService
import mineiros.mtracker.models.db.ContestsProblemasService
import mineiros.mtracker.models.db.ProblemasInfoService

fun Usuario.getContests() : List<Contest> = runBlocking {
    ContestsProblemasService()
        .allContests(nome)
        //.sortedBy { getTempoS(it) }
}

fun Usuario.getSortedContests() : List<Contest> =
    getContests().sortedByDescending { getTempoS(it) }

fun Usuario.getTags() : Map<String, Int> = runBlocking {
    val frequencyOfTags : MutableMap<String, Int> = mutableMapOf()
    for (contest in getContests())
        for (problema in ContestsProblemasService().problemasFromUserContest(nome, contest.id))
            for (tag in ProblemasInfoService().tags(problema.id))
                frequencyOfTags[tag] = (frequencyOfTags[tag] ?: 0) + 1
    frequencyOfTags
}

fun Usuario.setComentario(idContest: Int, comentario: String) = runBlocking {
    val contest = getContests().filter { it.id == idContest }.singleOrNull()
    if (contest == null)
        return@runBlocking ""
    else
        return@runBlocking setComentario(contest, comentario)
}

fun Usuario.setComentario(contest: Contest, comentario: String) = runBlocking {
    ContestsComentariosService().setComentario(nome, contest, comentario)
}

fun Usuario.getComentario(idContest: Int) : String = runBlocking {
    val contest = getContests().filter { it.id == idContest }.singleOrNull()
    if (contest == null)
        return@runBlocking ""
    else
        return@runBlocking getComentario(contest)
}

fun Usuario.getComentario(contest: Contest) : String = runBlocking {
    ContestsComentariosService().comentario(nome, contest)
}

fun Usuario.getTempoS(contest: Contest) : Int = runBlocking {
    ContestsComentariosService().tempoS(nome, contest)
}

fun Usuario.addContest(idContest: Int) : Unit = runBlocking {
    val allSubmissions = SubmissionRequests()
        .contestSubmissionsFromHandleId(handle, idContest)
    if(allSubmissions.isEmpty()) return@runBlocking

    ContestsProblemasService().addFromCFSubmission(nome, allSubmissions)

    val thisContest = getContests()
        .filter { it.id == idContest }
        .singleOrNull()
    if(thisContest != null) {
        ContestsComentariosService()
            .addComentarioInicial(nome, thisContest, allSubmissions.first().creationTimeSeconds)
    }
    for (sub in allSubmissions)
        ProblemasInfoService().addInfoFromCFSubmission(sub)

    val cfContest: CFContestCompact = ContestRequests().contestFromId(idContest)
    ContestsInfoService().addContest(
        id=cfContest.id,
        nome=cfContest.name,
        duracaoS=cfContest.durationSeconds
    )
}
