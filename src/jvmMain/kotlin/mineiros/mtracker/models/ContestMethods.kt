package mineiros.mtracker.models

import kotlinx.coroutines.runBlocking
import mineiros.mtracker.models.db.ContestsInfoService

fun Contest.getNome(): String? = runBlocking {
    ContestsInfoService().nome(id)
}

fun Contest.getDuracaoS(): Int? = runBlocking {
    ContestsInfoService().duracaoS(id)
}