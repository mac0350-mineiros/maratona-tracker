package mineiros.mtracker.models

class Relatorio (private val user: Usuario, private val contests: List<ContestShow>) {
    private val header = """
        # MAC0214 - Registro de Atividades
        
        Contests e problemas realizados como forma de estudo para competições de programação competitiva.
        
        Perfil no Codeforces: [${user.handle}](https://codeforces.com/profile/${user.handle})
        
        ---
        
    """.trimIndent()

    private fun contestToMD (contest: ContestShow): String {
        val url = "https://codeforces.com/" + if(contest.id >= 100001) { "gym/" } else { "contest/" } + "${contest.id}"

        var ret = """
            ## ${contest.data} - [${contest.nome}]($url)
            - Tempo total: ${contest.tempo?.div(3600)} hora(s) e ${contest.tempo?.div(60)?.mod(60)} minuto(s)
            - Problemas resolvidos:
            
        """.trimIndent()

        contest.problemas.forEach { ret += "  - [${it.id}: ${it.nome}]($url/problem/${it.id.last()})\n" }

        val comentario = user.getComentario(contest.id)
        if (comentario.isNotEmpty()){ ret += "- Comentário: $comentario\n" }

        ret += "\n---\n"

        return ret
    }

    private val tempo = contests.sumOf { it.tempo?:0 }

    private val total = """
        ## Tempo total: ${tempo.div(3600)} hora(s) e ${tempo.div(60).mod(60)} minuto(s)
        
    """.trimIndent()

    fun relatorio() : String {
        var ret = "$header\n"
        contests.forEach { ret += "${contestToMD(it)}\n" }
        ret += "\n$total"
        return ret
    }
}