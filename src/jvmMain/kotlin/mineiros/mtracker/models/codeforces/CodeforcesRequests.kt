package mineiros.mtracker.models.codeforces

import kotlinx.serialization.json.Json
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response

abstract class CodeforcesRequests() {

    protected val json = Json { ignoreUnknownKeys = true }
    // ignora campos que não existem na classe

    private val client = OkHttpClient()

    private fun apiCall(url: String): Response {
        val request = Request.Builder()
            .url(url)
            .build()

        val response = client.newCall(request).execute()
        return response
    }

    fun resultAsList(url: String) : String {
        val response = apiCall(url)
        val raw = json
            .decodeFromString<CFRaw>(response.body?.string() ?: """{status:"FAILED",result:[]}""")
        return raw.result.toString()
    }

    fun resultAsSingle(url: String) : String {
        val response = apiCall(url)
        val raw = json
            .decodeFromString<CFRawSingle>(response.body?.string() ?: """{status:"FAILED"}""")
        return raw.result.toString()
    }
}