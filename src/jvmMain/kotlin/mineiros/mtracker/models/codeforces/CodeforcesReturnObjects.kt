package mineiros.mtracker.models.codeforces

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement

@Serializable
data class CFRaw(val status: String,
                 val result: List<JsonElement>)

@Serializable
data class CFRawSingle(val status: String,
                       val result: JsonElement)

@Serializable
data class CFProblemCompact(val index: String?,
                            val name: String,
                            val tags: List<String>,)

@Serializable
data class CFRawContest(val contest: CFContestCompact,)

@Serializable
data class CFContestCompact(val id: Int,
                            val name: String,
                            val durationSeconds: Int,)

@Serializable
data class CFSubmissionCompact(val contestId: Int?,
                               val creationTimeSeconds: Int,
                               val problem: CFProblemCompact,
                               val verdict: String?,)