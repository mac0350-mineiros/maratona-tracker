package mineiros.mtracker.models.codeforces

class ContestRequests : CodeforcesRequests() {
    fun contestFromId(id: Int): CFContestCompact {
        val url = "https://codeforces.com/api/contest.standings?contestId=${id}&count=1"
        return contestFromJSON(resultAsSingle(url))
    }

    fun contestFromJSON(jsonText: String): CFContestCompact {
        val contest = json
            .decodeFromString<CFRawContest>(jsonText)
            .contest
        return contest
    }
}