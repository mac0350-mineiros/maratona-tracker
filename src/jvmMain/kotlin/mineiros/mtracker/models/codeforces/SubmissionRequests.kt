package mineiros.mtracker.models.codeforces

import kotlinx.serialization.json.JsonArray

class SubmissionRequests : CodeforcesRequests() {
    fun contestSubmissionsFromHandleId(handle: String, id: Int): ArrayList<CFSubmissionCompact> {
        val url = "https://codeforces.com/api/contest.status?contestId=${id}&handle=${handle}"
        return contestSubmissionsFromJSON(resultAsList(url))
    }

    fun contestSubmissionsFromJSON(jsonText: String): ArrayList<CFSubmissionCompact> {
        val subsArray = ArrayList<CFSubmissionCompact>()
        val submissions = json
            .decodeFromString<JsonArray>(jsonText)
        for (i in 0 until submissions.size) {
            val sub = json
                .decodeFromString<CFSubmissionCompact>(submissions[i].toString())
            subsArray.add(sub)
        }
        return subsArray
    }
}