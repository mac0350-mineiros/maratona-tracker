package mineiros.mtracker.models.db

import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.Table

const val MAX_NOME = 256

object Usuarios : Table() {
    val nome = varchar("nome", MAX_NOME)
    val handle = varchar("handle", MAX_NOME)

    override val primaryKey = PrimaryKey(nome)
}

object ContestsProblemas : Table() {
    val nomeU = varchar("nomeU", MAX_NOME)
        .references(Usuarios.nome, onDelete = ReferenceOption.CASCADE)
    val idContest = integer("idContest")
    val idProblema = varchar("idProblema", MAX_NOME)
    override val primaryKey = PrimaryKey(nomeU, idContest, idProblema)

}

const val MAX_COMENTARIO = 1024
object ContestsComentarios : Table() {
    val nomeU = varchar("nomeU", MAX_NOME)
        .references(Usuarios.nome, onDelete = ReferenceOption.CASCADE)
    val idContest = integer("idContest")
    val comentario = varchar("comentario", MAX_COMENTARIO)
    val tempoS = integer("tempoS")

    override val primaryKey = PrimaryKey(nomeU, idContest)
}

object ContestsInfo : Table() {
    val id = integer("id")
    val nome = varchar("nome", MAX_NOME)
    val duracaoS = integer("duracaoS")

    override val primaryKey = PrimaryKey(id)
}

object ProblemaNome : Table() {
    val id = varchar("id", MAX_NOME)
    val nome = varchar("nome", MAX_NOME)

    override val primaryKey = PrimaryKey(id)
}

object ProblemaTag : Table() {
    val id = varchar("id", MAX_NOME)
    val tag = varchar("tag", MAX_NOME)

    override val primaryKey = PrimaryKey(id,tag)
}