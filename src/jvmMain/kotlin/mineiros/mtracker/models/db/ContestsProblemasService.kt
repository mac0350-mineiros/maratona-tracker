package mineiros.mtracker.models.db

import mineiros.mtracker.models.Contest
import mineiros.mtracker.models.Problema
import mineiros.mtracker.models.codeforces.CFSubmissionCompact
import mineiros.mtracker.models.db.DatabaseSingleton.dbQuery
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll

interface ContestsProblemasInterface {
    suspend fun allContests(nomeUsuario: String): List<Contest>
    suspend fun addNewContest(nomeUsuario: String, contest: Contest)
    suspend fun deleteContest(nomeUsuario: String, contest: Contest) : Boolean
}

class ContestsProblemasService : ContestsProblemasInterface {
    suspend fun problemasFromUserContest(nomeUsuario: String, contestId: Int): List<Problema> = dbQuery {
        ContestsProblemas.selectAll()
            .where { (ContestsProblemas.nomeU eq nomeUsuario) and (ContestsProblemas.idContest eq contestId) }
            .map { Problema(it[ContestsProblemas.idProblema]) }
    }

    override suspend fun allContests(nomeUsuario: String): List<Contest> = dbQuery {
        ContestsProblemas.selectAll()
            .where { ContestsProblemas.nomeU eq nomeUsuario }
            .map { Contest(
                id = it[ContestsProblemas.idContest],
                problemas = problemasFromUserContest(it[ContestsProblemas.nomeU], it[ContestsProblemas.idContest])
            ) }
            .distinct()
    }

    override suspend fun addNewContest(nomeUsuario: String, contest: Contest) = dbQuery {
        deleteContest(nomeUsuario, contest) // importante para recadastrar o contest sem erro
        for(problema in contest.problemas) {
            ContestsProblemas.insert {
                it[nomeU] = nomeUsuario
                it[idContest] = contest.id
                it[idProblema] = problema.id
            }
        }
    }

    override suspend fun deleteContest(nomeUsuario: String, contest: Contest): Boolean = dbQuery {
        ContestsProblemas.deleteWhere { (nomeU eq nomeUsuario) and (idContest eq contest.id) } > 0
    }

    suspend fun addFromCFSubmission(nomeUsuario: String, submissions : List<CFSubmissionCompact>) {
        if(submissions.isEmpty()) return
        val id = submissions[0].contestId
        val problemas = submissions
            .filter{ it.verdict=="OK" }
            .distinct()
            .map { Problema(
                id=ProblemasInfoService().problemIdFromCFSubmission(it)
            ) }
        addNewContest(nomeUsuario, Contest(id!!, problemas))
        if (problemas.size > 0) {
            submissions
                .filter { it.verdict=="OK" }
                .distinct()
                .forEach { ProblemasInfoService()
                    .addProblemaNome(
                        ProblemasInfoService().problemIdFromCFSubmission(it),
                        it.problem.name
                    )
                }
        }
    }
}