package mineiros.mtracker.models.db

import mineiros.mtracker.models.codeforces.CFSubmissionCompact
import mineiros.mtracker.models.db.DatabaseSingleton.dbQuery
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll

interface ProblemasInfoInterface {
    suspend fun addProblemaNome(id: String, nome: String)
    suspend fun nome(id: String) : String?
    suspend fun addProblemaTag(id: String, tag: String)
    suspend fun addProblemaTag(id: String, tags: List<String>)
    suspend fun tags(id: String) : List<String>
}

class ProblemasInfoService : ProblemasInfoInterface {
    override suspend fun addProblemaNome(id: String, nome: String) = dbQuery {
        if (nome(id) != null) return@dbQuery
        ProblemaNome.insert {
            it[ProblemaNome.id] = id
            it[ProblemaNome.nome] = nome
        }
    }

    override suspend fun nome(id: String): String? = dbQuery {
        ProblemaNome.selectAll()
            .where { ProblemaNome.id eq id}
            .map { it[ProblemaNome.nome] }
            .singleOrNull()
    }

    override suspend fun addProblemaTag(id: String, tag: String) = dbQuery {
        if (tags(id).contains(tag)) return@dbQuery
        ProblemaTag.insert {
            it[ProblemaTag.id] = id
            it[ProblemaTag.tag] = tag
        }
    }

    override suspend fun addProblemaTag(id: String, tags: List<String>) {
        for (tag in tags)
            addProblemaTag(id, tag)
    }

    override suspend fun tags(id: String): List<String> = dbQuery {
        ProblemaTag.selectAll()
            .where { ProblemaTag.id eq id}
            .map { it[ProblemaTag.tag] }
    }

    fun problemIdFromCFSubmission(submission : CFSubmissionCompact) : String {
        return submission.contestId.toString()+submission.problem.index
    }

    suspend fun addInfoFromCFSubmission(submission: CFSubmissionCompact) = dbQuery {
        val problemId = problemIdFromCFSubmission(submission)
        addProblemaNome(problemId, submission.problem.name)
        addProblemaTag(problemId, submission.problem.tags)
    }
}