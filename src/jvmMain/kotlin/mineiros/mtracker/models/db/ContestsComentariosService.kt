package mineiros.mtracker.models.db

import mineiros.mtracker.models.Contest
import mineiros.mtracker.models.db.DatabaseSingleton.dbQuery
import org.jetbrains.exposed.sql.*

const val comentarioInicial: String = ""

interface ContestsComentariosInterface {
    suspend fun addComentarioInicial(nomeUsuario: String, contest: Contest, tempo: Int)
    suspend fun setComentario(nomeUsuario: String, contest: Contest, comentario: String)
    suspend fun comentario(nomeUsuario: String, contest: Contest): String
    suspend fun tempoS(nomeUsuario: String, contest: Contest) : Int
}

class ContestsComentariosService : ContestsComentariosInterface {
    override suspend fun addComentarioInicial(nomeUsuario: String, contest: Contest, tempo: Int) : Unit = dbQuery {
        ContestsComentarios.insert {
            it[nomeU] = nomeUsuario
            it[idContest] = contest.id
            it[comentario] = comentarioInicial
            it[tempoS] = tempo
        }
    }

    override suspend fun tempoS(nomeUsuario: String, contest: Contest) : Int = dbQuery {
        ContestsComentarios.selectAll()
            .where { (ContestsComentarios.idContest eq contest.id) and (ContestsComentarios.nomeU eq nomeUsuario) }
            .map { it[ContestsComentarios.tempoS] }
            .single()
    }

    override suspend fun setComentario(nomeUsuario: String, contest: Contest, comentario: String) : Unit = dbQuery {
        ContestsComentarios.update( {(ContestsComentarios.nomeU eq nomeUsuario) and (ContestsComentarios.idContest eq contest.id)} ){
            it[ContestsComentarios.comentario] = comentario.take(MAX_COMENTARIO)
        }
    }

    override suspend fun comentario(nomeUsuario: String, contest: Contest): String = dbQuery {
        ContestsComentarios.selectAll()
            .where { (ContestsComentarios.nomeU eq nomeUsuario) and (ContestsComentarios.idContest eq contest.id) }
            .map { it[ContestsComentarios.comentario] }
            .singleOrNull()?:""
    }
}