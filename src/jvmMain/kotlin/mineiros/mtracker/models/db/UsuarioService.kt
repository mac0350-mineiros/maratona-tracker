package mineiros.mtracker.models.db

import mineiros.mtracker.models.db.DatabaseSingleton.dbQuery

import kotlinx.coroutines.runBlocking
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import mineiros.mtracker.models.*
import org.jetbrains.exposed.sql.ResultRow

interface UsuarioInterface {
    suspend fun allUsuarios(): List<Usuario>
    suspend fun usuario(nome: String): Usuario?
    suspend fun addNewUsuario(nome: String, handle: String): Usuario?
    suspend fun deleteUsuario(nome: String): Boolean
}

class UsuarioService : UsuarioInterface {
    private fun resultRowToUsuario(row: ResultRow) = Usuario(
        nome = row[Usuarios.nome],
        handle = row[Usuarios.handle],
    )

    override suspend fun allUsuarios(): List<Usuario> = dbQuery {
        Usuarios.selectAll().map(::resultRowToUsuario)
    }

    override suspend fun usuario(nome: String): Usuario? = dbQuery {
        Usuarios.selectAll()
            .where { Usuarios.nome eq nome}
            .map(::resultRowToUsuario)
            .singleOrNull()
    }

    override suspend fun addNewUsuario(nome: String, handle: String): Usuario? = dbQuery {
        val insertStatement = Usuarios.insert {
            it[Usuarios.nome] = nome
            it[Usuarios.handle] = handle
        }
        insertStatement.resultedValues?.singleOrNull()?.let(::resultRowToUsuario)
    }

    override suspend fun deleteUsuario(nome: String): Boolean = dbQuery {
        Usuarios.deleteWhere { Usuarios.nome eq nome } > 0
    }
}

val dao: UsuarioService = UsuarioService().apply {
    runBlocking {
        if(allUsuarios().isEmpty()) {
            addNewUsuario("paulo", "rmm")
        }
    }
}