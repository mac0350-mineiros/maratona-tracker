package mineiros.mtracker.models.db

import mineiros.mtracker.models.db.DatabaseSingleton.dbQuery
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll

interface ContestsInfoInterface {
    suspend fun addContest(id: Int, nome: String, duracaoS: Int)
    suspend fun nome(id: Int) : String?
    suspend fun duracaoS(id: Int) : Int?
}

class ContestsInfoService : ContestsInfoInterface {

    override suspend fun addContest(id: Int, nome: String, duracaoS: Int) = dbQuery {
        if (nome(id) != null) return@dbQuery
        ContestsInfo.insert {
            it[ContestsInfo.id] = id
            it[ContestsInfo.nome] = nome
            it[ContestsInfo.duracaoS] = duracaoS
        }
    }

    override suspend fun nome(id: Int): String? = dbQuery {
        ContestsInfo.selectAll()
            .where { ContestsInfo.id eq id}
            .map { it[ContestsInfo.nome] }
            .singleOrNull()
    }

    override suspend fun duracaoS(id: Int): Int? = dbQuery {
        ContestsInfo.selectAll()
            .where { ContestsInfo.id eq id }
            .map { it[ContestsInfo.duracaoS] }
            .singleOrNull()
    }
}