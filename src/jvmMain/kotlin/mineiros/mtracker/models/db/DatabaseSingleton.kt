package mineiros.mtracker.models.db

import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction

object DatabaseSingleton {
    fun createHikariDataSource(
        url: String,
        driver: String
    ) = HikariDataSource(HikariConfig().apply{
        driverClassName = driver
        jdbcUrl = url
        maximumPoolSize = 3
        isAutoCommit = false
        transactionIsolation = "TRANSACTION_REPEATABLE_READ"
        validate()
    })
    fun init() {
        val driverClassName = "org.h2.Driver"
        val jdbcURL = "jdbc:h2:file:./build/db"
        val database = Database.connect(createHikariDataSource(jdbcURL, driverClassName))
        transaction(database){
            SchemaUtils.create(
                Usuarios,
                ContestsProblemas,
                ContestsComentarios,
                ContestsInfo,
                ProblemaTag,
                ProblemaNome
            )
        }
    }
    suspend fun <T> dbQuery(block: suspend () -> T): T =
        newSuspendedTransaction(Dispatchers.IO) { block() }

    fun addFirstUsuario() = runBlocking {
        dbQuery {
            /*
            val service = UsuarioService()
            service.deleteUsuario("pastel")
            service.addNewUsuario("pastel", "pastel")

            val usuario = Usuario("pastel", "pastel")

            usuario.addContest(1951)
            usuario.addContest(105020)

             */
        }
    }

}