package mineiros.mtracker.plugins

import com.google.gson.Gson
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.plugins.cors.routing.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import mineiros.mtracker.models.*
import mineiros.mtracker.models.db.UsuarioService
import mineiros.mtracker.models.db.dao
import java.text.SimpleDateFormat

fun Application.configureRouting() {
    install(CORS){
        allowHost("localhost:3000")
    }

    routing {
        get("/") {
            call.respondRedirect("usuarios")
        }
        route("usuarios") {
            get {
                call.respond(dao.allUsuarios())
            }
        }
        post("addContest") {
            val handle = call.parameters["handle"]
            val id = call.parameters["id"]

            if (handle == null || id == null) {
                call.respond(HttpStatusCode.BadRequest)
            } else {
                val service = UsuarioService()
                var user = service.usuario(handle)
                if (user == null) {
                    service.addNewUsuario(handle, handle)
                    user = service.usuario(handle)
                }
                user?.addContest(id.toInt())
                call.respond(HttpStatusCode.OK)
            }
        }
        get("comentario") {
            val handle = call.parameters["handle"]
            val id = call.parameters["id"]

            if (handle == null || id == null) {
                call.respond(HttpStatusCode.BadRequest)
            } else {
                val service = UsuarioService()
                var user = service.usuario(handle)
                if (user == null) {
                    service.addNewUsuario(handle, handle)
                    user = service.usuario(handle)
                }
                call.respond(user?.getComentario(id.toInt())?:HttpStatusCode.NoContent)
            }
        }
        post("setComentario") {
            val handle = call.parameters["handle"]
            val id = call.parameters["id"]
            val body = call.parameters["body"]
            if (handle == null || id == null || body == null) {
                call.respond(HttpStatusCode.BadRequest)
            } else {
                val service = UsuarioService()
                val user = service.usuario(handle)
                user?.setComentario(id.toInt(), body)
                call.respond(HttpStatusCode.OK)
            }
        }
        get("contests") {
            val handle = call.parameters["handle"]
            if (handle == null) {
                call.respond(HttpStatusCode.BadRequest)
            } else {
                val service = UsuarioService()
                var user = service.usuario(handle)
                if (user == null) {
                    service.addNewUsuario(handle, handle)
                    user = service.usuario(handle)
                }
                val contests = user?.getSortedContests()?.map{
                    ContestShow(
                        id=it.id,
                        nome=it.getNome()?:"",
                        tempo=it.getDuracaoS()?:0,
                        data=SimpleDateFormat("dd/MM/yyyy")
                            .format(user?.getTempoS(it)!!.toLong()*1000),
                        problemas=it.problemas.map{ problema ->
                            ProblemaShow(
                                problema.id,
                                problema.getNome()?:"Problema"
                            )
                        }
                    )
                }
                call.respond(Gson().toJson(contests))
            }
        }
        get("tags") {
            val handle = call.parameters["handle"]
            if (handle == null) {
                call.respond(HttpStatusCode.BadRequest)
            } else {
                val service = UsuarioService()
                var user = service.usuario(handle)
                if (user == null) {
                    service.addNewUsuario(handle, handle)
                    user = service.usuario(handle)
                }
                val gson = Gson()
                call.respond(gson.toJson(user?.getTags()))
            }
        }
        get("relatorio"){
            val handle = call.parameters["handle"]
            if(handle == null) {
                call.respond(HttpStatusCode.BadRequest)
            } else {
                val service = UsuarioService()
                var user = service.usuario(handle)
                if(user == null){
                    service.addNewUsuario(handle, handle)
                    user = service.usuario(handle)
                }

                val contests = user?.getSortedContests()?.map{
                    ContestShow(
                        id=it.id,
                        nome=it.getNome()?:"",
                        tempo=it.getDuracaoS()?:0,
                        data=SimpleDateFormat("dd/MM/yyyy")
                            .format(user?.getTempoS(it)!!.toLong()*1000),
                        problemas=it.problemas.map{ problema ->
                            ProblemaShow(
                                problema.id,
                                problema.getNome()?:"Problema"
                            )
                        }
                    )
                }

                val relatorio = user?.let { it1 -> Relatorio(it1, contests ?: listOf()) }

                if (relatorio != null) {
                    call.respond(relatorio.relatorio())
                } else {
                    call.respond(HttpStatusCode.InternalServerError)
                }
            }
        }
    }
}