package mineiros.mtracker

import io.ktor.server.application.*
import io.kvision.remote.kvisionInit
import mineiros.mtracker.models.db.DatabaseSingleton
import mineiros.mtracker.plugins.configureRouting

fun Application.main() {
    configureRouting()
    DatabaseSingleton.init()
    DatabaseSingleton.addFirstUsuario()
    kvisionInit()
}
